--- 
author: Céleste Clavier--Dupérat
title: Oral du brevet
subtitle: https://s.42l.fr/ccd
---

# 

<table>
    <tr>
        <td style="vertical-align: middle;">Céleste Clavier--Dupérat</td>     
        <td><img src="./includes/moi.jpg" style="width: 160px"> </td>   
    </tr> 
</table>


<small>

| | |
|-|-|
|<i class="fa fa-envelope"></i> | celeste@tcweb.org |
|<i class="fa fa-instagram"></i> | celeste.c_d |
|<i class="fa fa-phone"></i>    | +33 7 69 67 16 66 |

</small>

# Moi moi moi ... {data-background-image="./includes/moi.jpg" data-state="white70"}

* Céleste
* Clavier -- Dupérat

